﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRent
{
    interface IVehicle
    {
        int NrOfSeats { get; set; }
        void Go();
    }
}
