﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRent
{
    class Vehicle:IVehicle
    {
        public string Brand { get; set; }
        public string Model { get; set; }
        public string PlateNr { get; set; }
        public int NrOfSeats { get; set; }

        public Vehicle()
        {

        }
        public Vehicle(string brand, string model, string plateNr, int nrOfSeats)
        {
            Brand = brand;
            Model = model;
            PlateNr = plateNr;
            NrOfSeats = nrOfSeats;
        }

        public virtual void Go()
        {
            Console.WriteLine("brum brum");
        } //umżliwia nadpisanie

        public Vehicle(string brand, string model, string plateNr)
        {
            Brand = brand;
            Model = model;
            PlateNr = plateNr;
        }

    }
}
