﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRent
{
    class CarRental
    {
        List<VehicleToRent> VehiclesToRent {get;set;}

        public CarRental(List<VehicleToRent> vehiclesToRent)
        {
            VehiclesToRent = vehiclesToRent;
        }

        public Car RentACar(Client client)
        {
            if(client.DrivingLicence == true)
            {
                foreach(VehicleToRent vehicleToRent in VehiclesToRent)
                {
                    if (vehicleToRent.GetType() != typeof(Car))
                    {
                        continue;
                    }
                }
                //return ($"Client{client.Name} {client.Surname} has a driving licence.");
            }
            else
            {
                Console.WriteLine($"Client{client.Name} {client.Surname} hasn't a driving licence.");
                return null;
            }
            return new Car();
        }

        public string RentAScooter(Client client)
        {
            if (client.Age >= 18)
            {
                return ($"Client{client.Name} {client.Surname} is over 18.");
            }
            else
            {
                return ($"Client{client.Name} {client.Surname} isn't over 18.");
            }
        }

        public void AvaliableVehicles()
        {
            Console.WriteLine($"Aaliable vehicles:");
            int i = 0;
            foreach ( VehicleToRent vehicleToRent in VehiclesToRent){
                i++;
                if (vehicleToRent.IsAvaliable == true)
                {
                   //Console.WriteLine($"{i}");                
                   Console.WriteLine($"{(vehicleToRent.Vehicle as Vehicle).Brand}  | {(vehicleToRent.Vehicle as Vehicle).Model}");
                }
            }
            
        }

        public void FindPlateNr(string plateNr)
        {
            foreach (VehicleToRent vehicleToRent in VehiclesToRent)
            {
                if ((vehicleToRent.Vehicle as Vehicle) != null)
                {
                    //Console.WriteLine($"a");
                     if ((vehicleToRent.Vehicle as Vehicle).PlateNr == plateNr)
                    {
                        Console.WriteLine($"Vehicle with plates {plateNr} is {(vehicleToRent.Vehicle as Vehicle).Brand}  | {(vehicleToRent.Vehicle as Vehicle).Model}");
                    }
                }

            }

        }

    }
}
