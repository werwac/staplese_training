﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRent
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime dateOfBirthCl_1 = new DateTime(1986,2,26);
            DateTime dateOfBirthCl_2 = new DateTime(1986, 1, 10);
            DateTime dateOfBirthCl_3 = new DateTime(2003, 12, 10);

            Client cl_1 = new Client("John", "Smith", true, dateOfBirthCl_1);
            Client cl_2 = new Client("James", "Bond", false, dateOfBirthCl_2);
            Client cl_3 = new Client("Jane", "Smith", true, dateOfBirthCl_3);

            Car car_1 = new Car("Citroen", "Cactus", "123WWW", 5);
            Car car_2 = new Car("Fiat", "125p", "113KWW", 5);
            Scooter sco_1 = new Scooter("AA", "BB", "234KFD");

            List<VehicleToRent> vehiclesToRent_1=new List<VehicleToRent>();
            VehicleToRent v_1 = new VehicleToRent(car_1 as IVehicle ,true);
            VehicleToRent v_2 = new VehicleToRent(car_2 as IVehicle, true);
            VehicleToRent v_3 = new VehicleToRent(sco_1 as IVehicle, false);

            vehiclesToRent_1.Add(v_1);
            vehiclesToRent_1.Add(v_2);
            vehiclesToRent_1.Add(v_3);

            CarRental carRental_1 = new CarRental(vehiclesToRent_1);

            carRental_1.AvaliableVehicles();
            carRental_1.FindPlateNr("234KFD");
            Console.WriteLine(carRental_1.RentACar(cl_1));
            Console.WriteLine(carRental_1.RentACar(cl_2));
            Console.WriteLine(carRental_1.RentAScooter(cl_1));
            Console.WriteLine(carRental_1.RentAScooter(cl_3));

        }
    }
}
