﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRent
{
    class Client
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public Boolean DrivingLicence { get; private set; }

        DateTime DateOfBirth { get; set; }
        public int Age
        {
            get
            {
                return DateTime.Now.Year - DateOfBirth.Year;
            }
        }

        public Client(string name, string surname, Boolean drivingLicence, DateTime dateOfBirth)
        {
            Name = name;
            Surname = surname;
            DrivingLicence = drivingLicence;
            DateOfBirth = dateOfBirth;
        }

    }
}
