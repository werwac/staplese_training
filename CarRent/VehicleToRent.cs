﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRent
{
    class VehicleToRent
    {
        public IVehicle Vehicle { get; set; }
        public Boolean IsAvaliable { get; set; }
        public Client Client { get; set; }
        public DateTime? RentDate { get; set; } //nullable

        public VehicleToRent(IVehicle vehicle, Boolean isAvaliable)
        {
            Vehicle = vehicle;
            IsAvaliable = isAvaliable;
        }

        public VehicleToRent(IVehicle vehicle, Boolean isAvaliable, Client client, DateTime rentDate)
        {
            Vehicle = vehicle;
            IsAvaliable = isAvaliable;
            Client = client;
            RentDate = rentDate;
        }
    }
}
