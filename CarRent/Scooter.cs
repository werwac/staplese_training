﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRent
{
    class Scooter : Vehicle
    {
        public Scooter(string brand, string model, string plateNr) : base(brand, model, plateNr)
        {
            NrOfSeats = 2;
        }

        public override void Go()
        {
            Console.WriteLine("prr prr");
        }
    }
}
