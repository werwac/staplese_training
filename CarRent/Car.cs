﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRent
{
    class Car : Vehicle
    {
        public Car()
        {

        }
        public Car(string brand, string model, string plateNr, int nrOfSeats) : base(brand, model, plateNr, nrOfSeats)
        {
        }
    }
}
