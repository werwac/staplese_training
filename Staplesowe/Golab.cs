﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Staplesowe
{
    class Golab:Zwierze,IPtak
    {
        public bool CzyLata { get; set; }

        public Golab(string imie, string gatunek, string dzwiek, bool czyLata) : base(imie, gatunek, dzwiek)
        {
            CzyLata = czyLata;
        }

        public string Leć()
        {
            string chod = $"{Gatunek} {Imie} odeciał";
            return chod;
        }
    }
}
