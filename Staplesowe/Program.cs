﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Staplesowe
{
    class Program
    {
        static void Main(string[] args)
        {
            Golab golab = new Golab("Puszek", "gołąb", "Grrr", true);
            Kon kon = new Kon("Pimpuś","koń","Iooo",15);
            Krowa krowa = new Krowa("Mućka", "krowa", "Muu");
            Kura kura = new Kura("Genowefa","kura","PokPok",false);
            Mul mul = new Mul("Henio","muł","??",30);
            List<Zwierze> listaZwierzat1 = new List<Zwierze> {golab, kon, krowa, kura, mul};

            Farma farma = new Farma(listaZwierzat1);

            farma.ZwierzetaNaFarmie();
            farma.ZwierzetaPociagoweNaFarmie();
            farma.GlosyNaFarmie();
            farma.ZwierzetaLatajaceNaFarmie();

        }
    }
}
