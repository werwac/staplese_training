﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Staplesowe
{
    interface IZwierzePociagowe
    {
        double MaxCiezarTowaru { get; set; }

        string CiagnieWoz();
    }
}
