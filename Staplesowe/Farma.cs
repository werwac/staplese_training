﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Staplesowe
{
    class Farma
    {
        List<Zwierze> ListaZwierzat { get; set; }
        int IloscZwierzat { get; }

        public Farma(List<Zwierze> listaZwierzat)
        {
            ListaZwierzat = listaZwierzat;
            IloscZwierzat = ListaZwierzat.Count();
        }

        public void ZwierzetaNaFarmie()
        {
            Console.WriteLine();
            Console.WriteLine("Zwierzęta na farmie:");
            foreach (Zwierze zwierz in ListaZwierzat)
            {
                Console.WriteLine($"{zwierz.Gatunek} | {zwierz.Imie}"); 
            }
        }

        public void GlosyNaFarmie()
        {
            Console.WriteLine();
            Console.WriteLine("Głosy na farmie:");
            foreach (Zwierze zwierz in ListaZwierzat)
            {
                Console.WriteLine($"{zwierz.Gatunek} | {zwierz.Imie} | {zwierz.Dzwiek}");
            }
        }

        public void ZwierzetaPociagoweNaFarmie()
        {
            Console.WriteLine();
            Console.WriteLine("Zwierzęta pociągowe na farmie:");
            foreach (Zwierze zwierz in ListaZwierzat)
            {
                if(zwierz is IZwierzePociagowe)
                {
                    Console.WriteLine($"{zwierz.Gatunek} | {zwierz.Imie}");
                }  
            }
        }

        public void PtakiNaFarmie()
        {
            Console.WriteLine();
            Console.WriteLine("Ptaki na farmie:");
            foreach (Zwierze zwierz in ListaZwierzat)
            {
                if (zwierz is IPtak)
                {
                    Console.WriteLine($"{zwierz.Gatunek} | {zwierz.Imie} ");
                }
            }
        }

        //public void ZwierzetaLatajaceNaFarmie()
        //{
        //    int k = 0;
        //    List<int> j = new List<int> { };
        //    List<Zwierze> listaPtakow = new List<Zwierze> { };

        //    Console.WriteLine("Zwirzęta latające na farmie:");
        //    for (int i =0; i<ListaZwierzat.Count() ;i++)
        //    {
        //        if (ListaZwierzat[i] is IPtak)
        //        {
        //            j.Add(i);
        //            listaPtakow.Add(ListaZwierzat[i]);
        //        }
        //    }

        //    if (listaPtakow.Count > 0)
        //    {   
        //        foreach (IPtak ptak in listaPtakow)
        //        {
        //            if (ptak.CzyLata)
        //            {
        //                Console.WriteLine($"{ListaZwierzat[j[k]].Gatunek} | {ListaZwierzat[j[k]].Imie}");
        //            }
        //            k++;
        //        }
        //    }

        //}


        public void ZwierzetaLatajaceNaFarmie()
        {
            Console.WriteLine();
            Console.WriteLine("Zwirzęta latające na farmie:");
            foreach (Zwierze zwierz in ListaZwierzat)
            {
                if (zwierz is IPtak)
                {
                    if((zwierz as IPtak).CzyLata) //casting/rzutowanie
                    {
                        Console.WriteLine($"{zwierz.Gatunek} | {zwierz.Imie}");
                    }
                }
            }
        }

    }
}
