﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Staplesowe
{
    class Zwierze
    {
        public string Imie { get; set; }
        public string Gatunek { get; set; }
        public string Dzwiek { get; set; }

        public Zwierze(string imie, string gatunek, string dzwiek)
        {
            Imie = imie;
            Gatunek = gatunek;
            Dzwiek = dzwiek;
        }

        public string Je(string jedzenie)
        {
            string coJe = $"{Gatunek}  {Imie} je {jedzenie}.";
            return coJe;
        }

        public string WydajDzwiek()
        {
            string wydawanyDzwiek = $"{Gatunek}  {Imie}: {Dzwiek}";
            return wydawanyDzwiek;
        }

        public string Idzie()
        {
            string chodzi = $"{Gatunek}  {Imie} idzie.";
            return chodzi;
        }

    }
}
