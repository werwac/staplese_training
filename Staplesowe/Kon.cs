﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Staplesowe
{
    class Kon: Zwierze, IZwierzePociagowe
    { 
        public double MaxCiezarTowaru { get; set; }

        public Kon(string imie, string gatunek, string dziwek, double maxCiezarTowaru):base(imie,gatunek,dziwek)
        {
            MaxCiezarTowaru = maxCiezarTowaru;
        }

        string IZwierzePociagowe.CiagnieWoz()
        {
            string woz = $"{Gatunek} {Imie} ciagnie wóz.";
            return woz;
        }

        string Galopuje(int predkosc=10)
        {
            string galop = $"{Gatunek} {Imie} galopuje z prędkością {predkosc} km/h.";
            return galop;
        }
    }
}
