﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Staplesowe
{
    class Mul:Zwierze,IZwierzePociagowe
    {
        public double MaxCiezarTowaru { get; set; }

        public Mul(string imie, string gatunek, string dziwek, double maxCiezarTowaru):base(imie,gatunek,dziwek)
        {
            MaxCiezarTowaru = maxCiezarTowaru;
        }

        string IZwierzePociagowe.CiagnieWoz()
        {
            string woz = $"{Gatunek}  {Imie} ciagnie wóz.";
            return woz;
        }

        string Idzie(int predkosc = 3)
        {
            string chod = $" {Gatunek} {Imie} idzie z prędkością {predkosc} km/h.";
            return chod;
        }
    }
}
